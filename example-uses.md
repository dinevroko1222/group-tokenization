# Usage Examples

**Shorthand notation**

We will use the following notation to describe transactions in a way easy to
follow.

```
[Sequence number] <Transaction description>
    In
        <ptxo> // comment
    Out
        <utxo>
    Fee <fee>
```
Where we use the below shorthand notation to describe outputs:
```
    [BCH satoshi amount, Group ID, Group amount or authority flags]
```

## Simple Token Operations

With only Genesis, Baton, Mint, Melt


### Stablecoin Example

Note: Recall that OP_GROUP can be used together with any Script therefore any
step can be further secured with multisig.

Genesis transaction creates the first authority. If not enough authority is
given at genesis, we could be creating an useless group.

```
01 Genesis transaction
    In
        [ 10000]
    Out
        [  9000, Group ID = H(TX data), BATON | MINT | MELT]
    Fee    1000 // picked this amount for simplicity
```

We can then first split the authority, thus enabling separation of powers
between the issuance department and redeeming department of our hypothetical
organization.

```
02 Split authority
    In
        [  9000, Group ID, BATON | MINT | MELT]
    Out
        [  4000, Group ID, BATON | MINT]
        [  4000, Group ID, BATON | MELT]
    Fee    1000

```
We will then further secure the supply by first creating one-time use mint
authorities and then partially sign token mint transactions and hand them to
treasury department clerks.

Why not simply create the supply and give the balances to lower tiers of our
organization? Because blockchains are public. By securely storing the signed
partial transactions until they're actually needed we achieve two things:

- Hide the information on potential loot for potential adversaries.
- Keep the whole stablecoin more transparent, as the total supply on the
blockchain will more closely match the actual fiat amount received to back the
stablecoin.

```
03 Create one-time use mint authorities
    In
        [ 15000]
        [  4500, Group ID, BATON | MINT]
    Out
        [  9954] // BCH change
        [   546, Group ID, BATON | MINT] // preserve group authority baton
        [  2000, Group ID, MINT]
        [  2000, Group ID, MINT]
        [  2000, Group ID, MINT]
        [  2000, Group ID, MINT]
    Fee    1000
```
Notice how the amount of BCH freely flows in/out from grouped TXOs but it can't
go below the dust limit.

Next, we create TXes that can mint some fixed amount of our stablecoin, and
store these unpublished TXes in a vault. We must use the SIGHASH_ALL |
SIGHASH_ANYONECANPAY [signature type](https://reference.cash/protocol/blockchain/transaction/transaction-signing), otherwise an adversary could add more
token UTXOs and create infinite supply because with MINT in the inputs, such
transaction would still balance. These transactions are to be used at a later
time when teller hot wallets will need refilling. If there will be need to
increase the fee, the teller could add a BCH input to increase the fee.

```
Mint TX
    In
        [  2000, Group ID, MINT] // doesn't have BATON, so can't create another
                                 // MINT authority output
    Out
        [   546, Group ID, 10000000]
    Fee    1454
```
Should there be a suspicion that the vault has been compromised, those mint
authorities can be simply revoked by spending the UTXOs before the thief gets
a chance to use them.
```
Revocation TX
    In
        [  2000, Group ID, MINT]
    Out
        [  1000] // BCH change
    Fee    1000
```
Once mint TXes are published, tellers can distribute funds to clients using
ordinary token transfers. For client convenience, the token UTXOs can be further
charged with some BCH to allow them to move it wihout requiring external source
of BCH.

```
Ordinary Token Transfer
    In
        [ 10000]
        [   546, Group ID, 10000000]
    Out
        [  4500, Group ID,  1000000] // to client 1
        [  4500, Group ID,  2000000] // to client 2
        [   546, Group ID,  7000000] // teller change
    Fee    1000
```

### Atomic Swaps

Group tokenization allows multiple token and BCH TXOs in a single transaction
which enables any two parties to perform an atomic swap using existing Bitcoin
Cash transaction signing features.

If trading parties already discovered each other through some communication
channel they can agree in advance on all outputs then each sign their inputs
using SIGHASH_ALL and construct a transaction like the below.
```
Atomic Swap Transaction
    In
        [101000] // BCH signed by Bob
        [   546, Token A, 50000000] // Token A signed by Alice
    Out
        [100000] // BCH to Alice's address
        [   546, Token A, 50000000] // Token A to Bob's address
    Fee    1000
```
This allows for multi-token and multi-party trades, too. The below can be done
in the same way, as long as everyone can coordinate and agree on outputs in
advance.

```
Multi-token and multi-party Atomic Swap Transaction
    In
        [ 10000] // BCH signed by Alice
        [500000] // BCH signed by Bob
        [ 20000] // BCH signed by Carol
        [   546, Token A, 20000000] // Token A signed by Alice
        [   546, Token C, 10000000] // Token C signed by Carol
    Out
        [107908] // BCH to Alice's address (Trade 1 & BCH change)
        [117908] // BCH to Carol's address (Trade 1 & BCH change)
        [299000] // Bob's change (Trade 1)
        [   546, Token A,  2000000] // Token A to Bob's address (Trade 1)
        [   546, Token C,  1000000] // Token C to Bob's address (Trade 1)
        [   546, Token A,  4000000] // Token A to Carol's address (Trade 2)
        [   546, Token C,  2000000] // Token C to Alice's address (Trade 2)
        [   546, Token A, 14000000] // Alice's token change (Trade 1 & 2)
        [   546, Token C,  7000000] // Carol's token change (Trade 1 & 2)
    Fee    3000 // All participants agree to split the fee
```
Here Bob is buying two tokens at the same time and is paying 1/3 of the
transaction fee. Alice and Carol are trading with Bob (Trade 1) and with each
other (Trade 2) at the same time. Alice and Carol each pay for their share of
the fee and for the creation of additional token UTXOs. This transaction could
be made more compact by merging BCH and token UTXOs into group UTXOs storing
both the BCH and tokens. The outputs would then look like this:

```
    Out
        [299546, Token A,  2000000] // Bob's address
        [   546, Token C,  1000000] // Bob's address
        [108454, Token C,  2000000] // Alice's address
        [   546, Token A, 14000000] // Alice's address
        [118454, Token A,  4000000] // Carol's address
        [   546, Token C,  7000000] // Carol's address
```
Another possibility is to create a blind offer by crafting a partial transaction
and signing an input using SIGHASH_SINGLE. Suppose Bob wants to buy a token and
wants to post a bid offer. He will prepare a partial transaction like below and publish it to some public message board.

```
Blind Offer Atomic Swap Partial Transaction
    In
        [100000] // BCH signed by Bob
    Out
        [   546, Token T, 30000000] // Token T to Bob's address
```
Then, someone sees that offer and decides to take it. He will complete the transaction by adding his token to the inputs, and creating an output to take
the BCH offered.
```
Blind Offer Atomic Swap Completed Transaction
    In
        [100000] // BCH signed by Bob
        [   546, Token T, 30000000] // Token T signed by Taker
    Out
        [   546, Token T, 30000000] // Token T to Bob's address
        [099000] // BCH to Taker's address
    Fee
           1000
```
The Taker then posts the transaction and with that the trade is completed.
Should Bob change his mind, he can pull the offer by spending the input.
```
Blind Offer Atomic Swap Offer Cancellation Transaction
    In
        [100000] // BCH signed by Bob
    Out
        [099000] // BCH to Bob's address
    Fee
           1000 // Miners earn the fee even on cancelled trades :)
```

### CoinJoin

CoinJoin is another kind of atomic transaction. Instead of transferring value
between different owners, like with atomic swaps, it transfers value between
different storages of multiple owners and at the same time so it will obfuscate
which storage belongs to whom.

Simplest such transaction is with two parties. Suppose Alice and Bob both
received an amount of Token T from Eve. Now they don't want Eve to know who will
spend it first. They will then cooperate to construct a following transaction:
```
CoinJoin Transaction
    In
        [   546, Token T, 50000000] // Token T signed by Alice
        [   546, Token T, 50000000] // Token T signed by Bob
        [ 10000] // BCH for fee payment, signed by Alice
        [ 20000] // BCH for fee payment, signed by Bob
    Out
        [   546, Token T, 50000000] // Token T to Alice's new address
        [   546, Token T, 50000000] // Token T to Bob's new address
        [  9500] // BCH change to Alice's new address
        [ 19500] // BCH change to Bob's new address
    Fee    1000
```
This creates an anonimity set of two for every one of those outputs. Should an
output later be spent, Eve will have no way of knowing whether it's Alice or Bob
who's spending it, she could only be certain that it was one of them.

Alice and Bob could use the opportunity to split their token amounts into
smaller denominations, and then each of those outputs would have an anonimity
set of two. Of course, using any 2 together would link them to the same owner.
```
CoinJoin Transaction With Splitting Outputs
    In
        [   546, Token T, 50000000] // Token T signed by Alice
        [   546, Token T, 50000000] // Token T signed by Bob
        [ 10000] // BCH for fee payment, signed by Alice
        [ 20000] // BCH for fee payment, signed by Bob
    Out
        [   546, Token T, 25000000] // Token T to Alice's new address
        [   546, Token T, 25000000] // Token T to Alice's new address
        [   546, Token T, 25000000] // Token T to Bob's new address
        [   546, Token T, 25000000] // Token T to Bob's new address
        [  8954] // BCH change to Alice's new address
        [ 18954] // BCH change to Bob's new address
    Fee    1000
```
Notice that they both had to use a little BCH to provide the dust limit amount
for those new token outputs.

When mixing tokens, we still need some BCH to pay for the fees and new outputs
creation. Another way is to pre-charge the tokens with some BCH amount before
creating a CoinJoin transaction, and agree to have that amount always be
proportional to the token amount for any given round. Then, the CoinJoin
transaction could look like this:

```
CoinJoin Transaction With Splitting Outputs and using only token outputs
    In
        [ 10000, Token T, 50000000] // BCH & Token T signed by Alice
        [ 10000, Token T, 50000000] // BCH & Token T signed by Bob
    Out
        [  4750, Token T, 25000000] // BCH & Token T to Alice's new address
        [  4750, Token T, 25000000] // BCH & Token T to Alice's new address
        [  4750, Token T, 25000000] // BCH & Token T to Bob's new address
        [  4750, Token T, 25000000] // BCH & Token T to Bob's new address
    Fee    1000
```
This way, CoinJoin wouldn't differ much from how it's done with BCH. We imagine
that both CashShuffle and CashFusion servers could be extended to support
tokens, with little added complexity.

### NFTs

Trivial way to create a NFT is to create a new Group for it and mint exactly 1 token without preserving the MINT authority.

    TX01 Simple NFT Group Genesis
        In
            [10000]
        Out
            [  546, NFT ID = H(TX01), MINT] // One-time use mint authority
            [  546, NFT ID, MELT | BATON]   // Multi-use melt authority
            [ 7908]                         // BCH change
        Fee   1000

    TX02 Simple NFT Token Mint
        In
            [ 7908]
            [  546, NFT ID, MINT]           // One-time use mint authority
        Out
            [  546, NFT ID,    1]           // One unit of token with NFT ID
            [ 6908]                         // BCH change
        Fee   1000

Recall Group genesis scheme -- the NFT ID is computationally guaranteed to be unique because the 32-byte ID is pseudo-random and generated from genesis TX inputs data.
Supply is provably fixed because after minting the token no MINT authorities will exist because the only one will have been consumed in the TX02 above.
Interested parties can check for that by simply scanning the UTXO set for particular group MINT authorities.

The creator doesn't need to create the MELT | BATON authority, but it is a good way to allow provable destruction of the NFT.
With the BATON, the MELT authority can be transferred alongside the token.

#### NFT Metadata

The above example created a token, but no metadata was attached to it.
We could attach metadata by embedding it into an OP_RETURN of the genesis transaction, but that wouldn't give us a good way to update it.

If updating is required then one way would be to first create a NFT metadata token, where every TX of that token would write updated data into the OP_RETURN.
We'd then create the actual token and use the genesis OP_RETURN to link it to the metadata token.
To fetch the latest metadata interested parties could then simply look up the metadata token UTXO, which there will only be one of.
Metadata editing permission could be transferred simply by sending the metadata token to the new owner's address.

The above metadata scheme was just for example, and group token users could freely come up with something else as wallet standard.
The metadata scheme is beyond scope of Group Tokenization consensus layer CHIP.

#### NFTs using Subgroups

Subgroups allow for more versatility and convenience in managing NFTs.
First, we create new group with the full authority to operate on subgroups i.e. we create a genesis transaction that will create this authority output:

            [  546, PGID = H(TX),  MINT | MELT | BATON | SUBGROUP]

We can then use it to create any number of subgroup tokens or their authorities.
Recall that subgroups are group tokens with extended Group ID, where the extended bytes can be freely set if the required authority is present.
For example, we could create a series of trading cards with varying rarity. Note that the `+` operator in the example below indicates byte concatenation.

    Minting Multiple Subgroup Token Supplies
        In
            [10000]
            [  546, PGID,  MINT | MELT | BATON | SUBGROUP]
        Out
            [  546, PGID + 0x01,   10]
            [  546, PGID + 0x02,   20]
            [  546, PGID + 0x03,   50]
            [  546, PGID + 0x04,  100]
            [  546, PGID + 0x05,  200]
            [ 6816]
        Fee   1000

and then burn all SUBGROUP | MINT authorities of the parent group, provably locking the supply of subgroups and their tokens.
Because the group ID differs for that one extended byte they are distinct tokens and are not fungible

If the owner of parent group wants to delegate creation of distinct tokens to others, then instead of creating tokens themselves he can create subgroup authorities.

    Minting Multiple Subgroup MINT Authorities
        In
            [10000]
            [  546, PGID,  MINT | MELT | BATON | SUBGROUP]
        Out
            [  546, PGID + 0x01, MINT] // one-time use subgroup mint authority
            [  546, PGID + 0x02, MINT]
            [  546, PGID + 0x03, MINT]
            [  546, PGID + 0x04, MINT]
            [  546, PGID + 0x05, MINT]
            [ 6816]
        Fee   1000

To lock the series, the owner of parent group can simply burn his SUBGROUP authority.
This already happened in the transaction above because a "change" authority output wasn't included so all parent authorities got burned.
Subgroup authorities can then be used to create actual tokens when convenient.
Minting a subgroup token is done just like minting any other token:

    Minting a Single Subgroup Token Supply
        In
            [ 6816]
            [  546, PGID + 0x01, MINT]
        Out
            [  546, PGID + 0x01,   10]
            [ 5816]
        Fee   1000

This will automatically lock the supply to whatever amount was chosen, because we set it up so that the one-time use subgroup MINT authority was the only one in existence.

Recall that a parent group MINT authority without a SUBGROUP authority CAN NOT create subgroup tokens.
Burning the parent SUBGROUP authority makes the parent group just another group token, and it forever loses the capability to create subgroup tokens and authorities.

These tokens can then be freely used just like any other BCH and token outputs. Using the previous [Atomic Swap](#atomic-swaps) example, a DEX for NFT tokens could be created.
